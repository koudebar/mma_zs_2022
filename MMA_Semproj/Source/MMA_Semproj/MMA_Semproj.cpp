// Copyright Epic Games, Inc. All Rights Reserved.

#include "MMA_Semproj.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MMA_Semproj, "MMA_Semproj" );
