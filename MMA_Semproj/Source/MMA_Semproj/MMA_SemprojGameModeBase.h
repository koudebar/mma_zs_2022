// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MMA_SemprojGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MMA_SEMPROJ_API AMMA_SemprojGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
