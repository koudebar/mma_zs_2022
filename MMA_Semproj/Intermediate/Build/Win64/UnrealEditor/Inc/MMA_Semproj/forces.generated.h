// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MMA_SEMPROJ_forces_generated_h
#error "forces.generated.h already included, missing '#pragma once' in forces.h"
#endif
#define MMA_SEMPROJ_forces_generated_h

#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_SPARSE_DATA
#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_RPC_WRAPPERS
#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUforces(); \
	friend struct Z_Construct_UClass_Uforces_Statics; \
public: \
	DECLARE_CLASS(Uforces, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MMA_Semproj"), NO_API) \
	DECLARE_SERIALIZER(Uforces)


#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUforces(); \
	friend struct Z_Construct_UClass_Uforces_Statics; \
public: \
	DECLARE_CLASS(Uforces, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MMA_Semproj"), NO_API) \
	DECLARE_SERIALIZER(Uforces)


#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API Uforces(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(Uforces) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Uforces); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Uforces); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Uforces(Uforces&&); \
	NO_API Uforces(const Uforces&); \
public:


#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API Uforces(Uforces&&); \
	NO_API Uforces(const Uforces&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, Uforces); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(Uforces); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(Uforces)


#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_10_PROLOG
#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_SPARSE_DATA \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_RPC_WRAPPERS \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_INCLASS \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_SPARSE_DATA \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_INCLASS_NO_PURE_DECLS \
	FID_MMA_Semproj_Source_MMA_Semproj_forces_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MMA_SEMPROJ_API UClass* StaticClass<class Uforces>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_MMA_Semproj_Source_MMA_Semproj_forces_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
