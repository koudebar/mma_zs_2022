// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MMA_SEMPROJ_MMA_SemprojGameModeBase_generated_h
#error "MMA_SemprojGameModeBase.generated.h already included, missing '#pragma once' in MMA_SemprojGameModeBase.h"
#endif
#define MMA_SEMPROJ_MMA_SemprojGameModeBase_generated_h

#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_SPARSE_DATA
#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_RPC_WRAPPERS
#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMMA_SemprojGameModeBase(); \
	friend struct Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMMA_SemprojGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MMA_Semproj"), NO_API) \
	DECLARE_SERIALIZER(AMMA_SemprojGameModeBase)


#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAMMA_SemprojGameModeBase(); \
	friend struct Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AMMA_SemprojGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/MMA_Semproj"), NO_API) \
	DECLARE_SERIALIZER(AMMA_SemprojGameModeBase)


#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMMA_SemprojGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMMA_SemprojGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMMA_SemprojGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMMA_SemprojGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMMA_SemprojGameModeBase(AMMA_SemprojGameModeBase&&); \
	NO_API AMMA_SemprojGameModeBase(const AMMA_SemprojGameModeBase&); \
public:


#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMMA_SemprojGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMMA_SemprojGameModeBase(AMMA_SemprojGameModeBase&&); \
	NO_API AMMA_SemprojGameModeBase(const AMMA_SemprojGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMMA_SemprojGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMMA_SemprojGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMMA_SemprojGameModeBase)


#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_12_PROLOG
#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_SPARSE_DATA \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_RPC_WRAPPERS \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_INCLASS \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_SPARSE_DATA \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MMA_SEMPROJ_API UClass* StaticClass<class AMMA_SemprojGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
