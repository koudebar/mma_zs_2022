// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MMA_Semproj/forces.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeforces() {}
// Cross Module References
	MMA_SEMPROJ_API UClass* Z_Construct_UClass_Uforces_NoRegister();
	MMA_SEMPROJ_API UClass* Z_Construct_UClass_Uforces();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_MMA_Semproj();
// End Cross Module References
	void Uforces::StaticRegisterNativesUforces()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(Uforces);
	UClass* Z_Construct_UClass_Uforces_NoRegister()
	{
		return Uforces::StaticClass();
	}
	struct Z_Construct_UClass_Uforces_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Uforces_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_MMA_Semproj,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Uforces_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "forces.h" },
		{ "ModuleRelativePath", "forces.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Uforces_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Uforces>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_Uforces_Statics::ClassParams = {
		&Uforces::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_Uforces_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Uforces_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Uforces()
	{
		if (!Z_Registration_Info_UClass_Uforces.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_Uforces.OuterSingleton, Z_Construct_UClass_Uforces_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_Uforces.OuterSingleton;
	}
	template<> MMA_SEMPROJ_API UClass* StaticClass<Uforces>()
	{
		return Uforces::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(Uforces);
	struct Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_forces_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_forces_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_Uforces, Uforces::StaticClass, TEXT("Uforces"), &Z_Registration_Info_UClass_Uforces, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(Uforces), 2131765865U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_forces_h_45700580(TEXT("/Script/MMA_Semproj"),
		Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_forces_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_forces_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
