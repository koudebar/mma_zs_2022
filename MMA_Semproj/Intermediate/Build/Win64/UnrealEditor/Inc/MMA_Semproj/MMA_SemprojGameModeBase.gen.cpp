// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MMA_Semproj/MMA_SemprojGameModeBase.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMMA_SemprojGameModeBase() {}
// Cross Module References
	MMA_SEMPROJ_API UClass* Z_Construct_UClass_AMMA_SemprojGameModeBase_NoRegister();
	MMA_SEMPROJ_API UClass* Z_Construct_UClass_AMMA_SemprojGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_MMA_Semproj();
// End Cross Module References
	void AMMA_SemprojGameModeBase::StaticRegisterNativesAMMA_SemprojGameModeBase()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AMMA_SemprojGameModeBase);
	UClass* Z_Construct_UClass_AMMA_SemprojGameModeBase_NoRegister()
	{
		return AMMA_SemprojGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MMA_Semproj,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "MMA_SemprojGameModeBase.h" },
		{ "ModuleRelativePath", "MMA_SemprojGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMMA_SemprojGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::ClassParams = {
		&AMMA_SemprojGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMMA_SemprojGameModeBase()
	{
		if (!Z_Registration_Info_UClass_AMMA_SemprojGameModeBase.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AMMA_SemprojGameModeBase.OuterSingleton, Z_Construct_UClass_AMMA_SemprojGameModeBase_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AMMA_SemprojGameModeBase.OuterSingleton;
	}
	template<> MMA_SEMPROJ_API UClass* StaticClass<AMMA_SemprojGameModeBase>()
	{
		return AMMA_SemprojGameModeBase::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMMA_SemprojGameModeBase);
	struct Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AMMA_SemprojGameModeBase, AMMA_SemprojGameModeBase::StaticClass, TEXT("AMMA_SemprojGameModeBase"), &Z_Registration_Info_UClass_AMMA_SemprojGameModeBase, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AMMA_SemprojGameModeBase), 1337738205U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_2717069864(TEXT("/Script/MMA_Semproj"),
		Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_MMA_Semproj_Source_MMA_Semproj_MMA_SemprojGameModeBase_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
