// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMMA_Semproj_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_MMA_Semproj;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_MMA_Semproj()
	{
		if (!Z_Registration_Info_UPackage__Script_MMA_Semproj.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/MMA_Semproj",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0xAD5E050C,
				0x9B8A8EA3,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_MMA_Semproj.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_MMA_Semproj.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_MMA_Semproj(Z_Construct_UPackage__Script_MMA_Semproj, TEXT("/Script/MMA_Semproj"), Z_Registration_Info_UPackage__Script_MMA_Semproj, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0xAD5E050C, 0x9B8A8EA3));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
