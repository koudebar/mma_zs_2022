# MMA_ZS_2022

Tento repozitář slouží pro odevzdání semestrální práce pro předmět MMA na FEL ČVUT, ZS 2022.
Toto je simulace tekutin, konkrétně vytékajícího smaženého sýra.


## Členové týmu

* Matyáš Kovaľ <kovalmat@fel.cvut.cz>
* Barbora Koudelková <koudebar@fel.cvut.cz>



